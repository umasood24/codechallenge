<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers Email
 */
final class CodeChallengeTest extends TestCase
{
    public function testAreDateInputParametersValid(): void
    {
        $this->expectException(Exception::class);
        $code_challenge = new CodeChallenge();
        $code_challenge->getDatePeriod('06/08/2017 06:30 ', '06/150/2017 2:10', 'Australia/Adelaide', 'Asia/Karachi');
    }

    public function testDoesGetDatePeriodFunctionReturnDatePeriodInstance(): void
    {
        $code_challenge = new CodeChallenge();
        $this->assertInstanceOf(
            DatePeriod::class,
            $code_challenge->getDatePeriod('06/08/2017 06:30 ', '06/15/2017 2:10', 'Australia/Adelaide', 'Asia/Karachi')
    );
    }

    public function testCalculateNumberOfDaysWithoutTimeZone(): void
    {
        $code_challenge = new CodeChallenge();
        $this->assertCount(7, $code_challenge->challengePartOne('06/08/2017 06:30 ', '06/15/2017 2:10'));

    }

    public function testCalculateNumberOfDaysWithTimeZone(): void
    {
        $code_challenge = new CodeChallenge();
        $this->assertCount(8, $code_challenge->challengePartOne('06/08/2017 06:30 ', '06/15/2017 2:10', 'Australia/Adelaide', 'Asia/Karachi'));


    }

    public function testSolution4ConvertNoOfDayOfSolution1IntoSeconds(): void
    {
        $code_challenge = new CodeChallenge();
        $this->assertEquals(604800, $code_challenge->challengePartFour('06/08/2017 06:30 ', '06/15/2017 2:10', 'Australia/Adelaide', 'Asia/Karachi', 's'));
    }

    public function testSolution4ConvertNoOfDayOfSolution1IntoSecondsWithDTS(): void
    {
        $code_challenge = new CodeChallenge();
        $this->assertEquals(176400, $code_challenge->challengePartFour('04/01/2017 06:30 ', '04/03/2017 2:10', 'Australia/Adelaide', 'Asia/Karachi', 's'));
    }

    public function testSolution4ConvertNoOfDayOfSolution1IntoSecondsWithoutDTS(): void
    {
        $code_challenge = new CodeChallenge();
        $this->assertEquals(172800, $code_challenge->challengePartFour('04/04/2017 06:30 ', '04/06/2017 2:10', 'Australia/Adelaide', 'Asia/Karachi', 's'));
    }
}

