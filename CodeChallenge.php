<?php

/**
 * Created by PhpStorm.
 * User: umasood
 * Date: 7/6/17
 * Time: 9:51 PM
 */
class CodeChallenge
{
    function runChallenges()
    {

        try {
            echo('No. of days within the range : ' . $this->challengePartOne('06/08/2017 06:30 ', '06/15/2017 2:10', 'Australia/Adelaide', 'Asia/Karachi')->days . ' days!');
            echo '</br>';
            echo('No. of weekdays within the range : ' . $this->challengePartTwo('06/15/2017 06:24 ', '06/23/2017 1:56', 'Australia/Adelaide', 'Asia/Karachi') . ' days!');
            echo '</br>';
            echo('No. of complete weeks within the range : ' . $this->challengePartThree('06/01/2017 06:24 ', '06/19/2017 1:20', 'Australia/Adelaide', 'Asia/Karachi') . ' weeks!');
            echo '</br>';
            echo $this->challengePartFour('01/01/2017 06:30 ', '01/01/2018 2:10', 'Australia/Adelaide', 'Asia/Karachi', 'y');

        } catch (Exception $ex) {
            print_r($ex);
        }

    }

    public function getDatePeriod($date_from, $date_to, $time_zone_from = null, $time_zone_to = null)
    {
        try {

            if ($time_zone_from == null && $time_zone_to == null) {
                $date_from = new DateTime($date_from);
                $date_to = new DateTime($date_to);
            } else {
                $date_from = new DateTime($date_from, new DateTimeZone($time_zone_from));
                $date_to = new DateTime($date_to, new DateTimeZone($time_zone_to));
            }

            // 'P1D' ISO-8601 standard interval identifier that represents the interval of 1 day.
            // 'P1D' also factors in DTS and timezones
            $date_interval = new DateInterval('P1D');
            $date_period = new DatePeriod($date_from, $date_interval, $date_to);
            return $date_period;

        } catch (Exception $date_exception) {
            throw new Exception('Invalid date/time entry!');
        }
    }

    public function challengePartOne($date_from, $date_to, $time_zone_from = null, $time_zone_to = null)
    {
        $date_period = $this->getDatePeriod($date_from, $date_to, $time_zone_from, $time_zone_to);
        $week_days = [];

        foreach ($date_period as $date) {
            array_push($week_days,$date);
        }

        return $week_days;
    }

    public function challengePartTwo($date_from, $date_to, $time_zone_from = null, $time_zone_to = null)
    {
        try {
            $date_period = $this->getDatePeriod($date_from, $date_to, $time_zone_from, $time_zone_to);
            $week_days = 0;
            foreach ($date_period as $date) {
//                $date->format("N") returns day number of the week e.g 1 for Monday 2 for Tuesday etc
//                I have assumed here that the regular weekdays would be monday to friday.
                if ($date->format("N") < 6) {
                    $week_days++;
                }
            }
            return $week_days;

        } catch (Exception $date_exception) {
            throw new Exception('Invalid Date Parameters');

            //return 'Invalid date parameters!';ß
        }
    }

    public function challengePartThree($date_from, $date_to, $time_zone_from = null, $time_zone_to = null)
    {
        try {
            $date_period = $this->getDatePeriod($date_from, $date_to, $time_zone_from, $time_zone_to);
            $weeks = 0;
            foreach ($date_period as $date) {
                if ($date->format("N") == 1) {
                    $weeks++;
                }
            }
            return $weeks;

        } catch (Exception $date_exception) {
            throw new Exception("Invalid Date Parameters");
        }
    }

    /*Factor can be s -> Second, m -> Minute, h -> Hours , y -> Years */

    public function challengePartFour($date_from, $date_to, $time_zone_from = null, $time_zone_to = null, $factor)
    {
        $week_days = $this->challengePartOne($date_from, $date_to, $time_zone_from, $time_zone_to);

        $total_duration = 0;
        foreach($week_days as $i => $val){

            if(isset($week_days[$i+1])){
                $total_duration += $week_days[$i+1]->getTimestamp() - $week_days[$i]->getTimestamp();
            }

        }


        switch ($factor) {
            case 's':
                return $total_duration;
                break;
            case 'm':
                return ($total_duration / 60); //60 secs in a min
                break;
            case 'h':
                return ($total_duration /(60*60));
                break;
            case 'y':
                return ($total_duration / (60*60*24*365.25));
                break;
        }
    }

}